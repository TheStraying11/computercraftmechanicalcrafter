---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Zoë.
--- DateTime: 12/11/2023 02:26
---

---@param name string
---@return table
local function input(name)
    return peripheral.wrap(name)
end

---@param offset number
---@return table
local function crafters(offset)
    local rval = {{}, {}, {}, {}, {}, {}, {}, {}, {}}

    for i = 1, 9, 1 do
        for j=1, 9, 1 do
            local number = offset+(((j-1)*9)+(i-1))
            local template = "create:mechanical_crafter_%d"
            rval[i][j] = peripheral.wrap(template:format(number))
        end
    end

    return rval
end

return {
    crafters = crafters,
    input = input
}