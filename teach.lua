---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Zoë.
--- DateTime: 12/11/2023 02:37
---

local crafterLib = require("crafterLib")
local crafters = crafterLib.crafters(48)
local args = {...}

local recipe = {{}, {}, {}, {}, {}, {}, {}, {}, {}}

local function withOpen(func, filename, mode)
    local fh = fs.open(filename, mode)
    local rval = func(fh)
    fh.close()
    return rval
end

for i=1, 9 do
    for j=1, 9 do
        local status, name = pcall(function() return crafters[i][j].getItemDetail(1).name end)
        if status then
            recipe[i][j] = name
        else
            recipe[i][j] = ""
        end
    end
end

local recipes = withOpen(function(fh)
    return textutils.unserialize(fh.readAll())
end, "recipes.json", "r")

recipes[args[1]] = recipe
for k, v in pairs(recipes) do
    print(k, v)
end

withOpen(function(fh)
    fh.write(textutils.serializeJSON(recipes))
end, "recipes.json", "w")